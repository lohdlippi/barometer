const url = {
  production: 'https://api.paymetrics.co/',
  dev: 'http://localhost:3001/',
  test: 'https://barotest1.herokuapp.com/'
};

export default url.test;
