import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/pages/Landing'
import Dashboard from '@/pages/Dashboard'
import Register from '@/pages/Register'
import Login from '@/pages/Login'
import ForgotPassword from '@/pages/ForgotPassword'
import Settings from '@/pages/Settings'

Vue.use(Router)

const beforeEnter = (to, from, next) => {
  console.log(to)
  const decoded = window.localStorage.getItem('paymetrics')
  if (decoded) {
    next()
  } else {
    next('/login')
  }
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter: beforeEnter
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: ForgotPassword
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      beforeEnter: beforeEnter
    }
  ]
})
