import axios from 'axios'
import url from '../../utils/url'

export const state = () => ({
  authUser: null
})

export const mutations = {
  SET_USER: function(state, user) {
    state.authUser = user
  }
}

export const actions = {
  login({ commit }, { email, password }) {
    return axios({
      url: `/auth/login`,
      baseURL: url,
      method: 'post',
      data: {
        email,
        password
      }
    })
      .then(res => {
        const token = res.data.token
        window.localStorage.setItem('paymetrics', token)
        commit('SET_USER', res.data.user)
      })
      .catch(error => {
        const message = error.message ? error.message : 'There was an error'
        throw new Error(message)
      })
  },
  register({ commit }, { userData }) {
    return axios({
      url: `/auth/register`,
      baseURL: url,
      method: 'post',
      data: {
        email,
        password
      }
    })
      .then(res => {
        const token = res.data.token
        window.localStorage.setItem('paymetrics', token)
        commit('SET_USER', res.data.user)
      })
      .catch(error => {
        const message = error.message ? error.message : 'There was an error'
        throw new Error(message)
      })
  },
  logout({ commit }) {
    window.localStorage.removeItem('paymetrics')
    commit('SET_USER', null)
    window.location.replace('/')
  }
}

export default {
  state,
  actions,
  mutations
}
