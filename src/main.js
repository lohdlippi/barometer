// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import store from './store';
import AxiosPlugin from './plugins/axios';

import App from './App';
import router from './router';

Vue.config.productionTip = false;

Vue.use(AxiosPlugin);
import Trend from 'vuetrend';

Vue.use(Trend);

import Bars from 'vuebars';

Vue.use(Bars);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
